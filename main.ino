#include <BH1750.h>
#include <Wire.h>
#include <DHT.h>
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <WiFiUdp.h>
#include <TimeLib.h> 


//ConfiguraÃ§Ãµes Firebase
#define FIREBASE_HOST "devfest-iot-ff349.firebaseapp.com"
#define FIREBASE_AUTH "-------"

//ConfiguraÃ§Ãµes do Wifi
#define WIFI_SSID "---"
#define WIFI_PASSWORD "----"



//ConfiguraÃ§Ãµes dos Sensores
//--------------------------------
//Sensor de Temperatura e Umidade
#define DHTTYPE DHT11
#define DHT11_PIN 2

DHT dht(DHT11_PIN, DHTTYPE);

float temperature = 0;
float humidity = 0;

//Sensor de Luz
BH1750 lightMeter(0x23);

uint16_t lux = 0;


void setupFirebase() {

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void setupWifi() {

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Hey i 'm connecting...");

  while (WiFi.status() != WL_CONNECTED) {

    Serial.println(".");
    delay(500);
  }

  Udp.begin(localPort);
  setSyncProvider(getNtpTime);
  
  Serial.println();
  Serial.println("I 'm connected and my IP address: ");
  Serial.println(WiFi.localIP());
}


void printDigits(int digits){
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10){
  
    Serial.print('0');
  }
  Serial.print(digits);

}

String normalizeDigit(int digit){
  if(digit < 10){
     String aux = "0" + (String)digit;
     
     return aux;
  }else{
    return (String)digit;
  }
  
}

void setData(){

  // Pega a umidade e a temperatura do sensor DHT11  
    humidity = dht.readHumidity(); // em "%"
    temperature = dht.readTemperature(); // em "*C"g

  // Pega o nÃ­vel em lux do sensor BH1750
  lux = lightMeter.readLightLevel();

  StaticJsonBuffer<100> jsonBuffer2;
  JsonObject &root2 = jsonBuffer2.createObject();
  root2["temperatura"] = temperature;

  StaticJsonBuffer<100> jsonBuffer3;
  JsonObject &root3 = jsonBuffer3.createObject();
  root3["umidade"] = humidity;

  StaticJsonBuffer<100> jsonBuffer4;
  JsonObject &root4 = jsonBuffer4.createObject();
  root4["lux"] = lux;
  
  String temperatura = "temperatura";
  String umidade = "umidade";
  String luminosidade = "luminosidade";
  
  Firebase.push(temperatura, root2);
  Firebase.push(umidade, root3);
  Firebase.push(luminosidade, root4);

  if(Firebase.failed()){
    Serial.print("Erro ao ler o decibeis");
    Serial.println(Firebase.error());
  }



  Serial.print(humidity); Serial.print(" %\t\t");
  Serial.print(temperature); Serial.print(" *C\t");
  Serial.print(maxVolume); Serial.print(" dB(A)\t");
  Serial.print(lux); Serial.print(" lx\t");
  i=0;
  maxVolume = 0;
  delay(1000);
}

void setup()
{
  
  Serial.begin(9600);
  Wire.begin(12,14);
  lightMeter.begin();
  dht.begin();
  setupWifi();
  setupFirebase();


 
  Serial.println("RH\t\tTemp (C)\tRuido (dB)\tLux (lx)");
}

void loop()
{
  setData();
  delay(1000);

}

